<?php

namespace Database\Factories;

use App\Models\Comentario;
use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Comentario>
 */
class ComentarioFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comentario::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'Posts_id' => Post::inRandomOrder()->first()->id,
            'contenido' => $this->faker->text('500'),
            'fecha_creacion' => $this->faker->dateTime(),
            'fecha_actualizacion' => $this->faker->dateTime()
        ];
    }
}
