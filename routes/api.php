<?php

use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\ComentarioController;
use App\Http\Controllers\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Categoria routes
Route::post('categoria/', [CategoriaController::class, 'create']);
Route::put('categoria/{id}', [CategoriaController::class, 'update']);
Route::delete('categoria/{id}', [CategoriaController::class, 'delete']);

Route::get('categoria/edit/{id}', [CategoriaController::class, 'edit']);
Route::get('categoria/ver/{id}', [CategoriaController::class, 'ver']);
Route::get('categoria', [CategoriaController::class, 'verTodo']);

//Post routes
Route::post('post', [PostController::class, 'create']);
Route::put('post/{id}', [PostController::class, 'update']);
Route::delete('post/{id}', [PostController::class, 'delete']);

Route::get('post/edit/{id}', [PostController::class, 'edit']);
Route::get('post/ver/{id}', [PostController::class, 'ver']);
Route::get('post', [PostController::class, 'verTodo']);


//Comentario routes
Route::post('comentario/', [ComentarioController::class, 'create']);
Route::put('comentario/{id}', [ComentarioController::class, 'update']);
Route::delete('comentario/{id}', [ComentarioController::class, 'delete']);


Route::get('comentario/edit/{id}', [ComentarioController::class, 'edit']);
Route::get('comentario/ver/{id}', [ComentarioController::class, 'ver']);
Route::get('comentario', [ComentarioController::class, 'verTodo']);
