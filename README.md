## About apiBlog

Instrucciones para el uso e instalacion de la API

-   Eliminar exmple del archivo .env (.env.example)
-   Usar el comando composer install, para instalar las dependencias
-   Crear la base de datos en mysql
-   Asignar el nombre de la base de datos en el archivo .env
-   Correr las migraciones: php artisan migrate
-   Correr los seeders: php artisan db:seed

Eso son los pasos necesario para utilizar la aplicacion

## Routas API

## Comentarios:

Parametros = contenido, Posts_id;

Listar todos los comentarios: (get)

-   **[http://d-veloper.host/apiBlog/public/api/comentario]**

Listar un comentario: (get)

-   **[http://d-veloper.host/apiBlog/public/api/comentario/ver/$id]**

Editar un comentario: (get)

-   **[http://d-veloper.host/apiBlog/public/api/comentario/edit/$id]**

Agregar un comentario: (post)

-   **[http://d-veloper.host/apiBlog/public/api/comentario/]**

Actualizar un comentario: (put)

-   **[http://d-veloper.host/apiBlog/public/api/comentario/$id]**

Eliminar un comentario: (delete)

-   **[http://d-veloper.host/apiBlog/public/api/comentario/$id]**

## Post:

Parametros = titulo, contenido, Categorias_id;

Listar todos los Post: (get)

-   **[http://d-veloper.host/apiBlog/public/api/post]**

Listar un Post: (get)

-   **[http://d-veloper.host/apiBlog/public/api/post/ver/$id]**

Editar un Post: (get)

-   **[http://d-veloper.host/apiBlog/public/api/post/edit/$id]**

Agregar un post: (post)

-   **[http://d-veloper.host/apiBlog/public/api/post/]**

Actualizar un post: (put)

-   **[http://d-veloper.host/apiBlog/public/api/post/$id]**

Eliminar un post: (delete)

-   **[http://d-veloper.host/apiBlog/public/api/post/$id]**

## Categorias:

Parametros = nombre;

Listar todos los categoria: (get)

-   **[http://d-veloper.host/apiBlog/public/api/categoria]**

Listar una categoria: (get)

-   **[http://d-veloper.host/apiBlog/public/api/categoria/ver/$id]**

Editar una categoria: (get)

-   **[http://d-veloper.host/apiBlog/public/api/categoria/edit/$id]**

Agregar una categoria: (post)

-   **[http://d-veloper.host/apiBlog/public/api/categoria/]**

Actualizar una categoria: (put)

-   **[http://d-veloper.host/apiBlog/public/api/categoria/$id]**

Eliminar una categoria: (delete)

-   **[http://d-veloper.host/apiBlog/public/api/categoria/$id]**
